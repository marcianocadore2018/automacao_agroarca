Dado("que esteja na página de tipo produto") do
  autentica_login
  @menu.acessa_tipo_produto
  expect(@type_product.title_page.text).to eql 'Tipo Produtos'
  @first_value = @type_product.value_description_table.first.text
end

Quando("clicar no botão novo tipo produto") do
  @type_product.new_type_product
end

Então("devo ser direcionado para o formulário de cadastro do tipo de produto") do
  expect(@type_product.title_new_type_product.text).to eql 'Novo Tipo Produto'
end

Então("devo preencher a descricao do tipo de produto") do
  @value_type_product = 'adiciona tipo de produto'
  @type_product.input_description.set(@value_type_product)
end

Então("clicar no botão Criar Tipo Produto") do
  @type_product.btn_create_type_product
end

Então("devo ser direcionado para a listagem de tipo produto") do
  expect(@type_product.value_description_table.first.text).to eq(@value_type_product)
end

Quando("devo tentar realizar o cadastro de um tipo produto já existente") do
  @type_product.input_description.set(@first_value)
  @type_product.btn_create_type_product
end

Quando("clicar no botão editar o primeiro registro da tabela tipo produto") do
  @edit_type_product = @type_product.value_description_table.first.text
  @type_product.btn_edit_type_product
end

Então("devo ser direcionado para o formulário de cadastro do tipo de produto visualizando o valor no campo descrição") do
  expect(@type_product.input_description_edit.value).to eq(@edit_type_product)
end

Então("clicar no botão Salvar Tipo Produto") do
  @type_product.save_update_type_product
end

E('devo alterar a descrição do tipo do produto informando o valor {string}') do |description|
  @update_description_type_product = description
  @type_product.input_description.set(@update_description_type_product)
end

Então("devo ser direcionado para a listagem de tipo produto visualizando o registro atualizado") do
  expect(@type_product.value_description_table.first.text).to eq(@update_description_type_product)
end

Quando("clicar no botão excluir o primeiro registro da tabela confirmando a exclusão do tipo de produto") do
  @value_deleted = @type_product.value_description_table.first.text
  @type_product.delete_type_product
end

Então("não devo visualizar mais o registro do tipo de produto que foi excluído") do
  expect(@type_product.value_description_table.first.text).not_to eq(@value_deleted)
end

Então('a mensagem para o tipo de produto já existente deverá ser {string}') do |descripition|
  expect(@type_product.message_duplicate.text).to include(descripition)
end