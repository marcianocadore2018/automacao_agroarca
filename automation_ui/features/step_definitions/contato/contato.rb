Dado("que esteja na página de contato") do
  autentica_login
  @menu.access_contact
  expect(@contact.title_page.text).to eql 'Contatos'
end

Quando("clicar no botão novo contato") do
  @contact.new_contact
end

Então("devo ser direcionado para o formulário de contato") do
  expect(@contact.title_new_contact.text).to eql 'Criar Contato'
end

Então("devo preencher o formulário do contato selecionando o tipo de pessoa {string} campo CNPJ {string} razão social e inscriação estadual deve vir preenchido") do |tp_person, cnpj|
  @contact.select_tp_person.select(tp_person).click
  @contact.input_cnpj.set(cnpj)
  @contact.input_representative_name.click
  sleep 2
  expect(@contact.input_company_name.value).not_to be_empty
  expect(@contact.input_state_registration.value).not_to be_empty
end

Então("devo informar o campo nome do representante, email, celular, telefone") do
  @contact.input_representative_name.set('nome do representante')
  @contact.input_email.set('emailteste@teste.com.br')
  @contact.input_representative_name.set('nome do representante')
  @contact.input_cell_phone.set('(54)9944-1587')
  @contact.input_phone.set('(45)4644-6465')
end

Então("clicar no botão Criar Contato") do
  @contact.btn_create_contact
  sleep 2
  @contact.btn_confirm_modal
end

Então("clicar no botão Criar Contato para tentar cadastrar o contato") do
  @contact.btn_create_contact
  sleep 2
end

Então('devo clicar no botão voltar contato') do
  @contact.back_contact
end

E('devo preencher o formulário do contato selecionando o tipo de pessoa {string} campo CPF que já existe, nome, data nascimento {string}') do |tp_person, birth_date|
  @contact.select_tp_person.select(tp_person).click
  @contact.input_name_contact.set(Faker::Name.name)
  @contact.input_cpf.set(@cpf)
end

Então("clico no botão Criar Contato para cadastrar um usuário já existente") do
  @contact.btn_create_contact
  sleep 2
end

Então('deverá aparecer a mensagem do cpf {string}') do |message|
  expect(@contact.cpf_duplicate.text).to eq(message)
end

Então('deverá aparecer a mensagem do cnpj {string}') do |message|
  expect(@contact.cnpj_duplicate.text).to eq(message)
end

Então("o contato deverá ser adicionar e no início do formulário as mensagens {string} {string} deverão ser exibidas") do |message_one, message_two|
  expect(@contact.alert_modal_contact.text).to include(message_one)
  expect(@contact.alert_modal_contact.text).to include(message_two)
end

Quando("clicar no botão Adicionar Categoria") do
  @contact.btn_add_category.click
  sleep 2
  expect(@contact.title_modal_category.text).to eq('Adicionar Categoria')
end

E("selecionar uma categoria clicando no botão adicionar") do
  @category_added = @contact.value_category_added.first.text
  @contact.list_add_category.first.click
  sleep 1.5
end

Então("deverei visualizar a categoria adicionada") do
  expect(@contact.table_category_contact.first.text).to eq(@category_added)
end

Quando("clicar em Adicionar Endereço") do
  @contact.btn_add_address.click
end

Então("os campos cep, cidade, endereço, bairro, número deverão estar preenchidos pois estou informando um cnpj válido") do
  sleep 2
  expect(@contact.input_cep.value).not_to be_empty
  expect(@contact.select_city.value).not_to be_empty
  expect(@contact.input_address.value).not_to be_empty
  expect(@contact.input_district.value).not_to be_empty
  expect(@contact.input_number.value).not_to be_empty
end

E('deverei informar os campos observação, cep, endereço, bairro, número, complemento da modal para realizar o cadastro do contato da pessoa física') do
  sleep 3
  @address_added = 'observação endereço PF'
  @contact.address_observation_pf.set(@address_added)
  @contact.input_cep.set('99050-144')
  sleep 3
  @contact.input_number.set('1182')
end

E("quando preencher os campos obsevação, complemento") do
  @address_added = 'observação do endereço'
  @contact.input_note.set(@address_added)
  @contact.input_complement.set('complemento teste')
  @contact.input_number.set('1187')
end

E("clicar no botão Criar Endereço") do
  @contact.btn_create_address.click
  sleep 2
  @contact.confirm_modal.click
end

Então("os valores informados na modal de criar endereço deverão ser exibidas") do
 sleep 3
 expect(@contact.list_address_value.first.text).to include(@address_added)
end

Quando("clicar no botão Adicionar informação de Contato") do
  @contact.btn_add_info_contact
end


E("informar os campos nome, contato, observação") do
  @name_notes = 'Nome observação'
  @contact.input_name_info_contact.set(@name_notes)
  @contact.input_contact.set('contato descricao')
  @contact.input_note_contact.set('observacao de contato')
end


E("clicar no botão Criar Informação de Contato") do
  @contact.create_info_contact
end

Então("deverá ser exibida a modal de confirmação") do
  @contact.alert_modal_info_contact.click
end

E("as informações digitadas deverão ser exibidas na tabela") do
  expect(@contact.table_info_contact.first.text).to include(@name_notes)
end

E('devo preencher o formulário do contato selecionando o tipo de pessoa {string} campo CPF nome, data nascimento {string}') do |tp_person, birth_date|
  @contact.select_tp_person.select(tp_person).click
  @contact.input_name_contact.set(Faker::Name.name)
  @cpf = Faker::CPF.pretty
  @contact.input_cpf.set(@cpf)
end

E('devo selecionar o gênero {string} email, celular, telefone') do |gender|
  @contact.gender.click
  @contact.input_email.set(Faker::Internet.email)
  @contact.input_cell_phone.set('(54)9999-4444')
  @contact.input_phone.set('(54)9999-2222')
  @contact.birth_date.set('01/12/1991')
  #Rever esse campo
  @contact.input_state_registration.set(Faker::Name.name[0...12])
end

E("informar os campos nome, contato, observação da pessoa física") do
  sleep 3
  @name_notes = 'Nome observação'
  @contact.input_name_information_contact.last.set(@name_notes)
  @contact.input_contact.set('contato descricao')
  @contact.input_note_contact.set('observacao de contato')
end

Quando('clicar no botão excluir o último registro de contato da tabela confirmando a exclusão') do
  @quantity_row_table_contact = @contact.quantity_row_table.size
  @contact.delete_contact
end

Então('a quantidade de contatos da tabela deverá ser alterada pois o registro foi removido') do
  expect(@contact.quantity_row_table.size).to eq(@quantity_row_table_contact - 1)
end

E('clicar no botão editar o último registro de contato da tabela') do
  @update_contact = @contact.btn_edit_contact
end

Então("devo ser direcionado para o formulário de alteração de contato com o valor no campo descrição") do
  expect(@contact.title_new_contact.text).to include('Editar contato:')
end

Então("devo alterar o nome, email, celular, telefone") do
  @new_name = @contact.input_name_contact.set(Faker::Name.name).value.downcase
  @new_email = @contact.input_email.set(Faker::Internet.email).value.downcase
  @new_cellphone = @contact.input_cell_phone.set('(54)9999-2222').value.downcase
  @new_phone = @contact.input_phone.set('(54)9999-1111').value.downcase

end

Quando("clicar no botão Salvar Contato após clicar no botão ok para confirmar a atualização") do
  @contact.save_update_contact
end

Então("os valores nome, data nascimento, email, celular, telefone do contato deverão ter sido alterados") do
  page.refresh
  expect(@contact.input_name_contact.value.downcase).to eq(@new_name)
  expect(@contact.input_email.value.downcase).to eq(@new_email)
  expect(@contact.input_cell_phone.value.downcase).to eq(@new_cellphone)
  expect(@contact.input_phone.value.downcase).to eq(@new_phone)
end

Quando('clicar no botão excluir do registro do cnpj do google') do
 @quantity_row_table_contact = @contact.quantity_row_table.size
 @contact.delete_contact_pj
end