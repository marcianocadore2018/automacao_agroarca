
Dado("que esteja na página de categoria") do
  autentica_login
  @menu.access_category
  expect(@category.title_page.text).to eql 'Categorias'
  @first_value = @category.value_description_table.first.text
end

Quando('clicar no botão editar o primeiro registro da tabela') do
  @edit_category = @category.value_description_table.first.text
  @category.btn_edit_category
end

Quando('clicar no botão excluir o primeiro registro da tabela') do
  @delete_category = @category.value_description_table.first.text
  @category.btn_delete_category
end

Quando("clicar no botão nova categoria") do
  @category.new_category
end

Então("devo ser direcionado para o formulário de cadastro") do
  expect(@category.title_new_category.text).to eql 'Nova Categoria'
end

Então("devo preencher a descricao da categoria") do
  @value_category = 'a categoria'
  @category.input_description.set(@value_category)
end

Então("clicar no botão Criar Categoria") do
  @category.btn_create_category
end

Então("devo ser direcionado para a listagem de categoria") do
  expect(@category.value_description_table.first.text).to eq(@value_category)
end

E('devo alterar a descrição informando o valor {string}') do |description|
  @update_description_category = description
  @category.input_description.set(description)
end

E('clicar no botão Salvar Categoria') do
  @category.save_update_category
end

Então('devo ser direcionado para o formulário de cadastro visualizando o valor no campo descrição') do
  expect(@category.input_description_edit.value).to eq(@edit_category)
end

Então('devo ser direcionado para a listagem de categoria visualizando a categoria alterada') do
  expect(@category.value_description_table.first.text).to eq(@update_description_category)
end

E('clicar no botão excluir o primeiro registro da tabela confirmando a exclusão') do
  @value_deleted = @category.value_description_table.first.text
  @category.delete_category
end

Então('não devo visualizar mais o registro que foi excluído') do
  expect(@category.value_description_table.first.text).not_to eq(@value_deleted)
end

E('devo tentar realizar o cadastro de uma categoria já existente') do
  @category.input_description.set(@first_value)
  @category.btn_create_category
end

Então('devo visualizar a mensagem {string}') do |descripition|
  expect(@category.message_duplicate.text).to include(descripition)
end