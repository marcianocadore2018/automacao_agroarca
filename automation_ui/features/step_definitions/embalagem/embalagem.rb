Dado('que esteja na página de embalagens') do
  autentica_login
  @menu.acessa_embalagens
  expect(@packing.title_page.text).to eql 'Embalagens'
  @first_value = @packing.value_description_table.first.text
end

Quando('clicar no botão Nova Embalagem') do
  @packing.new_packing
end

Então('devo ser direcionado para o formulário de cadastro de embalagem') do
  expect(@packing.title_new_packing.text).to eql 'Nova Embalagem'
end

Então('devo preencher a descricao da embalagem') do
  @value_packing = 'adiciona embalagem'
  @packing.input_description.set(@value_packing)
end

Então('clicar no botão Criar Embalagem') do
  @packing.btn_create_packing
end

Então('devo ser direcionado para a listagem de embalagens') do
  expect(@packing.value_description_table.first.text).to eq(@value_packing)
end

Quando('devo tentar realizar o cadastro de uma embalagem já existente') do
  @packing.input_description.set(@first_value)
  @packing.btn_create_packing
end

Então('a mensagem para a embalagem já existente deverá ser {string}') do |message|
  expect(@packing.message_duplicate.text).to include(message)
end

Quando('clicar no botão editar o primeiro registro da tabela embalagens') do
  @edit_packing = @packing.value_description_table.first.text
  @packing.btn_edit_packing
end

Então('devo ser direcionado para o formulário de cadastro de embalagem visualizando o valor no campo descrição') do
  expect(@packing.input_description_edit.value).to eq(@edit_packing)
end

Então('devo alterar a descrição da embalagem informando o valor {string}') do |description|
  @update_description_packing = description
  @packing.input_description.set(@update_description_packing)
end

Então('clicar no botão Salvar Embalagem') do
  @packing.save_update_packing
end

Então('devo ser direcionado para a listagem de embalagens visualizando o registro atualizado') do
  expect(@packing.value_description_table.first.text).to eq(@update_description_packing)
end

Quando('clicar no botão excluir o primeiro registro da tabela confirmando a exclusão da embalagem') do
  @value_deleted = @packing.value_description_table.first.text
  @packing.delete_packing
end

Então('não devo visualizar mais a embalagem que foi excluída') do
  expect(@packing.value_description_table.first.text).not_to eq(@value_deleted)
end
