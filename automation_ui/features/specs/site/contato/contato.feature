# language:pt

@agroarca
@contato
Funcionalidade: Cadastros > Contato
  Eu como QA do agroarca
  Quero realizar testes no menu cadastros - Contatos
  Para que eu possa validar se as funcionalidades do módulo estão corretas

  @cadastro_contato_pj
  Cenario: Cadastro de contato pessoa jurídica
    Dado que esteja na página de contato
    Quando clicar no botão novo contato
    Então devo ser direcionado para o formulário de contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "Pessoa Jurídica" campo CNPJ "06.990.590/0001-23" razão social e inscriação estadual deve vir preenchido
    E devo informar o campo nome do representante, email, celular, telefone
    E clicar no botão Criar Contato
    Então o contato deverá ser adicionar e no início do formulário as mensagens "Adicione uma categoria ao contato" "Adicione um endereço ao contato" deverão ser exibidas
    Quando clicar no botão Adicionar Categoria
    E selecionar uma categoria clicando no botão adicionar
    Então deverei visualizar a categoria adicionada
    Quando clicar em Adicionar Endereço
    Então os campos cep, cidade, endereço, bairro, número deverão estar preenchidos pois estou informando um cnpj válido
    E quando preencher os campos obsevação, complemento
    E clicar no botão Criar Endereço
    Então os valores informados na modal de criar endereço deverão ser exibidas
    Quando clicar no botão Adicionar informação de Contato
    E informar os campos nome, contato, observação
    E clicar no botão Criar Informação de Contato
    Então deverá ser exibida a modal de confirmação
    E as informações digitadas deverão ser exibidas na tabela

  @contato_ja_existe_pj
  Cenario: Contato Pessoa Jurídica já existe
    Dado que esteja na página de contato
    Quando clicar no botão novo contato
    Então devo ser direcionado para o formulário de contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "Pessoa Jurídica" campo CNPJ "06.990.590/0001-23" razão social e inscriação estadual deve vir preenchido
    E devo informar o campo nome do representante, email, celular, telefone
    E clico no botão Criar Contato para cadastrar um usuário já existente
    Então deverá aparecer a mensagem do cnpj "O CNPJ já está cadastrado."
    
  @cadastro_contato_pf
  Cenario: Cadastro de contato pessoa física
    Dado que esteja na página de contato
    Quando clicar no botão novo contato
    Então devo ser direcionado para o formulário de contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "Pessoa Física" campo CPF nome, data nascimento "21/03/1992"
    E devo selecionar o gênero "M" email, celular, telefone
    E clicar no botão Criar Contato
    Então o contato deverá ser adicionar e no início do formulário as mensagens "Adicione uma categoria ao contato" "Adicione um endereço ao contato" deverão ser exibidas
    Quando clicar no botão Adicionar Categoria
    E selecionar uma categoria clicando no botão adicionar
    Então deverei visualizar a categoria adicionada
    Quando clicar em Adicionar Endereço
    Então deverei informar os campos observação, cep, endereço, bairro, número, complemento da modal para realizar o cadastro do contato da pessoa física
    E clicar no botão Criar Endereço
    Então os valores informados na modal de criar endereço deverão ser exibidas
    Quando clicar no botão Adicionar informação de Contato
    E informar os campos nome, contato, observação da pessoa física
    E clicar no botão Criar Informação de Contato
    Então deverá ser exibida a modal de confirmação
    E as informações digitadas deverão ser exibidas na tabela

  @cpf_ja_existente
  Cenario: Contato Pessoa Física já existe
    Dado que esteja na página de contato
    Quando clicar no botão novo contato
    Então devo ser direcionado para o formulário de contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "Pessoa Física" campo CPF nome, data nascimento "21/03/1992"
    E devo selecionar o gênero "M" email, celular, telefone
    E clicar no botão Criar Contato
    Então devo clicar no botão voltar contato
    Quando clicar no botão novo contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "Pessoa Física" campo CPF que já existe, nome, data nascimento "21/03/1992"
    E devo selecionar o gênero "M" email, celular, telefone
    E clicar no botão Criar Contato para tentar cadastrar o contato
    Então deverá aparecer a mensagem do cpf "O CPF já está cadastrado."

  @exclusao_contato_cnpj
  Cenário: Exclusão de contato CNPJ
    Dado que esteja na página de contato
    Quando clicar no botão excluir do registro do cnpj do google
    Então a quantidade de contatos da tabela deverá ser alterada pois o registro foi removido

  @exclusao_contato
  Cenário: Exclusão de contato
    Dado que esteja na página de contato
    Quando clicar no botão excluir o último registro de contato da tabela confirmando a exclusão
    Então a quantidade de contatos da tabela deverá ser alterada pois o registro foi removido

  @altera_contato
  Cenário: Altera Contato
    Dado que esteja na página de contato
    Quando clicar no botão editar o último registro de contato da tabela
    Então devo ser direcionado para o formulário de alteração de contato com o valor no campo descrição
    E devo alterar o nome, email, celular, telefone
    Quando clicar no botão Salvar Contato após clicar no botão ok para confirmar a atualização
    Então os valores nome, data nascimento, email, celular, telefone do contato deverão ter sido alterados