# language:pt

@agroarca
@embalagem
Funcionalidade: Estoque > Embalagens
  Eu como QA do agroarca
  Quero realizar testes no menu estoque - Embalagens
  Para que eu possa validar se as funcionalidades do módulo estão corretas

  @cadastro_embalagem
  Cenário: Cadastro de embalagem
    Dado que esteja na página de embalagens
    Quando clicar no botão Nova Embalagem
    Então devo ser direcionado para o formulário de cadastro de embalagem
    E devo preencher a descricao da embalagem
    E clicar no botão Criar Embalagem 
    Então devo ser direcionado para a listagem de embalagens
  
  @embalagem_ja_existente
  Cenário: Tentar cadastrar uma embalagem que já está cadastrada
    Dado que esteja na página de embalagens
    Quando clicar no botão Nova Embalagem 
    E devo ser direcionado para o formulário de cadastro de embalagem
    E devo tentar realizar o cadastro de uma embalagem já existente
    Então a mensagem para a embalagem já existente deverá ser "Essa embalagem já está cadastrada"

  @alteracao_embalagem
  Cenário: Alteração de embalagem
    Dado que esteja na página de embalagens
    Quando clicar no botão editar o primeiro registro da tabela embalagens
    Então devo ser direcionado para o formulário de cadastro de embalagem visualizando o valor no campo descrição
    E devo alterar a descrição da embalagem informando o valor "alteração da embalagem 1"
    E clicar no botão Salvar Embalagem
    Então devo ser direcionado para a listagem de embalagens visualizando o registro atualizado

  @exclusao_embalagem
  Cenário: Exclusão de embalagem
    Dado que esteja na página de embalagens
    Quando clicar no botão excluir o primeiro registro da tabela confirmando a exclusão da embalagem
    Então não devo visualizar mais a embalagem que foi excluída