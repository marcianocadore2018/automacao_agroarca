require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'site_prism'
require "faker"
require "cpf_faker"
require "capybara_table/rspec"


Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  config.app_host = "http://testes.agroarca.com.br"
end