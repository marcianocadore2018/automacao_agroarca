# encoding: utf-8

class ContactPage < SitePrism::Page
  
  element  :title_page, '.box-body h1'
  element  :title_new_contact, '.container-fluid h3'
  elements :value_description_table, '.data-table tr td a'
  elements :btn_new_contact, 'a.btn.btn-success'
  elements :edit_contact, '.btn-editar'
  elements :btn_delete_contact, '.btn.btn-danger.fa.fa-times'
  element  :btn_update_contact, 'input.btn.btn-success'
  element  :btn_confirm_exclusion, '.sweet-confirm.styled'
  element  :message_duplicate, '.box-body .container-fluid'
  element  :select_tp_person, '#tipo_pessoa'
  element  :confirm_modal, '.sweet-confirm.styled'
  element  :cnpj_duplicate, '#form-contato > div.row.campo-pj > div:nth-child(1) > div'
  element  :cpf_duplicate, '#form-contato > div:nth-child(7) > div.form-group.col-md-6.campo-pf > div'

  #base contact
  element :input_name_contact, '#nome'
  element :input_cpf, '#cpf'
  element :input_cnpj, '#cnpj'
  element :birth_date, '#data_nascimento'
  element :gender, '#genero-m'
  element :input_company_name, '#razao_social'
  element :input_state_registration, '#inscricao_estadual'
  element :input_representative_name, '#nome_representante'
  element :input_email, '#email'
  element :input_cell_phone, '#celular'
  element :input_phone, '#telefone'
  element :alert_modal_contact, '.box-body .alert.alert-warning'

  #modal category
  element  :btn_add_category, '#adicionar-categoria'
  element  :title_modal_category, '#modal-categorias-title'
  elements :list_add_category, '.btn-adicionar'
  elements :btn_close_category, '.btn.btn-default'
  elements :value_category_added, '.adicionar-categoria.in .table tbody tr'
  elements :table_category_contact, '#contato-categorias .lista-container .table td'

  #modal address
  element  :btn_add_address, '#adicionar-endereco'
  element  :input_note, '#nome'
  element  :input_cep, '#cep'
  element  :select_city, 'select#cidade_id'
  element  :input_address, '#endereco'
  element  :input_district , '#bairro'
  element  :input_number, '#numero'
  element  :input_complement , '#complemento'
  element  :btn_create_address, '#criar-endereco'
  elements :list_address_value, '#contato-enderecos tr td'
  element  :address_observation_pf, '#modal-enderecos #nome'

  #modal information contact
  element  :title_modal_info_contact, '#modal-informacoes-title'
  element  :input_name_info_contact, '#nome'
  elements :input_name_information_contact, '#nome'
  element  :input_contact, '#info_contato'
  element  :input_note_contact, '#modal-informacoes #observacao'
  element  :alert_modal_info_contact, '.show-sweet-alert.visible .sweet-confirm.styled'
  elements :table_info_contact, '#contato-informacoes .table-responsive tbody tr td'
  element  :btn_back_contact, '#form-contato > div:nth-child(15) > a'

  #table contact
  elements :quantity_row_table, 'tbody tr'
  elements :btn_delete_contact, '.btn.btn-danger'

  def new_contact
    btn_new_contact.last.click
  end

  def back_contact
    btn_back_contact.click
  end

  def btn_create_contact
    click_button 'Criar Contato'
  end

  def create_info_contact
    click_button 'Criar Informação de Contato'
  end

  def btn_add_info_contact
    click_button 'Adicionar Informação de Contato'
  end

  def btn_confirm_modal
    sleep 1.5
    confirm_modal.click
    sleep 1.5
  end
  
  def btn_edit_contact
    edit_contact.last.click
  end

  def delete_contact
    btn_delete_contact.last.click
    sleep 1.5
    btn_confirm_exclusion.click
    sleep 1.5
    btn_confirm_exclusion.click
  end

  def delete_contact_pj
    btn_delete_contact.first.click
    sleep 1.5
    btn_confirm_exclusion.click
    sleep 1.5
    btn_confirm_exclusion.click
  end

  def save_update_contact
    btn_update_contact.click
    sleep 2
    click_button 'OK'
  end
end
