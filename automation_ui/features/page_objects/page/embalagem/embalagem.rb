# encoding: utf-8

class PackingPage < SitePrism::Page
  
    element  :title_page, '.box-body h1'
    element  :title_new_packing, '.container-fluid h3'
    element  :input_description, '#descricao'
    elements :value_description_table, '.data-table tr td a'
    elements :btn_new_packing, 'a.btn.btn-success'
    elements :edit_packing, '.btn-editar'
    elements :btn_delete_packing, '.btn.btn-danger.fa.fa-times'
    element  :input_description_edit, '#descricao'
    element  :btn_update_packing, 'input.btn.btn-success'
    element  :btn_confirm_exclusion, '.sweet-confirm.styled'
    element  :message_duplicate, '.box-body .container-fluid'
  
    def new_packing
      btn_new_packing.last.click
    end
  
    def btn_create_packing
      click_button 'Criar Embalagem'
    end
    
    def btn_edit_packing
      edit_packing.first.click
    end
  
    def delete_packing
      btn_delete_packing.first.click
      sleep 1.5
      btn_confirm_exclusion.click
      sleep 1.5
      btn_confirm_exclusion.click
    end
  
    def save_update_packing
      btn_update_packing.click
    end
  end
  