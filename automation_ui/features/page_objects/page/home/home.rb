# encoding: utf-8

class HomePage < SitePrism::Page
    element :logo_menu, '.logo-lg b'
    element :menu_profile, '.fa.fa-user.user-image'

    def logout
      click_link 'Sair'
    end
end
  